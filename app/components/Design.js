import styles from './Design.css';

const fs = require('fs');
const math = require('mathjs');
const paper = require('paper');
const path = require('path');
const PropTypes = require('prop-types');
const React = require('react');
const TextPath = require('./TextPath');
const Rectangle = require('./Rectangle');
const Circle = require('./Circle');

class Design extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      liked: false
    };

    this.toggleEnabled = this.toggleEnabled.bind(this);

    this.tokens = props.tokens;
    this.creative = props.creative;
    if (this.creative != null) {
      this.width = this.creative.canvasWidth;
      this.height = this.creative.canvasWidth;
    } else {
      this.width = this.height = 400;
    }

    this.elements = [];

    const canvas = document.createElement('canvas');
    canvas.onclick = this.toggleEnabled;
    canvas.width = this.width;
    canvas.height = this.height;
    canvas.className = 'disliked';

    this.canvas = canvas;
    this.scope = new paper.PaperScope();
    this.scope.setup(canvas);
    document.getElementById('sketches').appendChild(canvas);
  }

  toggleEnabled() {
    this.canvas.className = '';
    this.state.liked = !this.state.liked;
    this.canvas.classList.add(this.state.liked ? 'liked' : 'disliked');
  }

  export(svg = true) {
    if (this.state.liked) {
      if (svg) {
        const out = path.resolve(`app/results/result_${Date.now()}.svg`);
        const image = this.scope.project.exportSVG();
        const data = new XMLSerializer().serializeToString(image);
        const buffer = new Buffer(data);
        fs.writeFileSync(out, buffer);
      } else {
        const out = path.resolve(`app/results/result_${Date.now()}.png`);
        const image = this.canvas.toDataURL();
        const data = image.replace(/^data:image\/\w+;base64,/, '');
        const buffer = new Buffer(data, 'base64');
        fs.writeFileSync(out, buffer);
      }
    }
  }

  toMatrix() {
    return this.elements.map((element) => {
      const bounds = element.bounds;
      return [bounds.x, bounds.y, bounds.width, bounds.height, element._class];
    });
  }

  addTitle(tokens) {
    const tokenCount = tokens.length;
    const textElements = [];

    for (let i = 0; i < tokenCount; i += 1) {
      const textPath = new TextPath({
        design: this,
        textString: tokens[i],
        initOrder: 'front'
      });
      textElements.push(textPath.element);
    }

    if (tokenCount > 1) {
      const group = new paper.Group(this.elements);
      group.position = this.scope.view.center;
    }
  }

  addRectangle() {
    this.scope.activate();
    const rectangle = new Rectangle({
      design: this
    });
    rectangle.element.sendToBack();
    this.elements.push(rectangle.element);
  }

  addCircle() {
    this.scope.activate();
    const circle = new Circle({
      design: this
    });
    circle.element.sendToBack();
    this.elements.push(circle.element);
  }
}

Design.propTypes = {
  tokens: PropTypes.array,
  creative: PropTypes.object
}

module.exports = Design;
