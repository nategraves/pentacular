const React = require('react');

const Design = require('./Design');
const Tool = require('./Tool');

class CircleTool extends Tool {
  constructor(props) {
    super(props);

    this.addCircle = this.addCircle.bind(this);
    this.seedCircles = this.seedCircles.bind(this);
  }

  addCircle() {
    const creative = this.creative;
    if (creative.designs.length > 0) {
      const enabledDesigns = creative.designs.filter((design) => design.state.liked);
      for (let i = 0; i < enabledDesigns.length; i += 1) {
        enabledDesigns[i].addCircle(creative);
      }
    }
  }

  seedCircles() {
    const creative = this.creative;
    for (let i = 0; i < 12; i += 1) {
      const design = new Design({ creative });
      design.addCircle(creative);
      creative.designs.push(design);
    }
  }

  render() {
    return (
      <div className="text-center tool">
        <h2><i className="fa fa-circle"></i></h2>
        <button onClick={this.seedCircles} className="text-button">Generate</button>
        <button onClick={this.addCircle} className="text-button">Add</button>
      </div>
    );
  }
}

module.exports = CircleTool;
