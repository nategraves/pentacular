// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import styles from './Home.css';

export default class Home extends Component {
  render() {
    return (
      <div>
        <Link to="/train">Train</Link>
        <Link to="/create">Create</Link>
      </div>
    );
  }
}
