const paper = require('paper');
const math = require('mathjs');
const Item = require('./Item');

class Circle extends Item {
  constructor(props) {
    super(props);

    const size = (props.size != null) ? props.size : this.randomSize();
    const position = (props.position != null) ? props.position : this.randomPosition();
    const color = (props.color != null) ? props.color : this.randomRGB();

    this.element = new paper.Path.Circle(position, size);
    this.element.fillColor = color;

    if (this.initOrder === 'front') {
      this.element.bringToFront();
    } else {
      this.element.sendToBack();
    }
  }

  randomSize() {
    const d = this.design;
    return math.randomInt(0, math.round(math.min(d.width, d.height), 10));
  }
}

module.exports = Circle;
