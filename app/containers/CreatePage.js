// @flow
import React, { Component } from 'react';
import Create from '../components/Create';

class CreatePage extends Component {
  render() {
    return (
      <Create />
    );
  }
}

module.exports = CreatePage;
