const React = require('react');
const PropTypes = require('prop-types');

class Tool extends React.Component {
  constructor(props) {
    super(props);
    this.creative = props.creative;

    if (this.constructor === Tool) {
      throw new TypeError(`Cannot directly instantiate ${this.constructor.name} class`);
    }
  }
}

Tool.propTypes = {
  creative: PropTypes.object.isRequired
};

module.exports = Tool;
