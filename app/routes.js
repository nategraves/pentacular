/* eslint flowtype-errors/show-errors: 0 */
import React from 'react';
import { Switch, Route } from 'react-router';
import App from './containers/App';
import HomePage from './containers/HomePage';
import CreatePage from './containers/CreatePage';
import TrainingPage from './containers/TrainingPage';

export default () => (
  <App>
    <Switch>
      <Route path="/create" component={CreatePage} />
      <Route path="/train" component={TrainingPage} />
      <Route path="/" component={HomePage} />
    </Switch>
  </App>
);
