const paper = require('paper');
const Item = require('./Item');

class Rectangle extends Item {
  constructor(props) {
    super(props);

    const size = (props.size != null) ? props.size : this.randomSize();
    const position = (props.position != null) ? props.position : this.randomPosition();
    const color = (props.color != null) ? props.color : this.randomRGB();

    this.element = new paper.Path.Rectangle(position, size);
    this.element.fillColor = color;

    if (this.initOrder === 'front') {
      this.element.bringToFront();
    } else {
      this.element.sendToBack();
    }
  }
}

module.exports = Rectangle;
