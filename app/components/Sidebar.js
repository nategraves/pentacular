var React = require('react');

class Sidebar extends React.Component {
  render() {
    return (
      <div className="sidebar">
        <div className="main">
          <form onSubmit={this.processText}>
            <input type="text" value={this.state.value} onChange={this.updateText} />
            <input type="submit" className="btn btn-primary" value="Go" />
          </form>
        </div>
      </div>
    )
  }
}

module.exports = Sidebar;
