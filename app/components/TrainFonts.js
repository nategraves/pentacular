const fs = require('fs');
const math = require('mathjs');
const path = require('path');
const React = require('react');
const coolstory = require('coolstory.js');

const Design = require('./Design');

class TrainFonts extends React.Component {
  constructor(props) {
    super(props);
    this.prepTitleRound = this.prepTitleRound.bind(this);
  }

  prepTitleRound() {
    const fonts = fs.readdirSync(path.resolve('app/fonts'));
    const selectedFonts = math.pickRandom(fonts, 2);
    const title = coolstory.title(60);
    const tokens = title.split(' ');
    const tokenCount = tokens.length;
    const breakMatrix = new Array(tokens.length);

    for (let i = 0; i < tokenCount; i += 0) {
      breakMatrix[i] = (i + 1) / tokenCount;
    }

    this.generateTitleDesigns(12, tokens);
  }

  generateTitleDesigns(count, tokens) {
    const designs = [];
    for (let i = 0; i < count; i += 1) {
      const layout = (tokens.length > 2) ? 1 : math.randomInt(0, 1);
      const design = new Design();
      const paths = [];


      if (layout) {
        // Vertical
      } else {
        // Horizontal
      }
    }

    return designs;
  }

  /*
  prepParagraphRound() {

  }
  */

  render() {
    return (
      <div>
        <div>TrainFonts</div>
        <button onClick={this.prepTitleRound}>Start</button>
      </div>
    );
  }
}

module.exports = TrainFonts;
