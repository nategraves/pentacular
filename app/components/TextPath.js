const fs = require('fs');
const math = require('mathjs');
const opentype = require('opentype.js');
const path = require('path');
const paper = require('paper');
const Item = require('./Item');

class TextPath extends Item {
  constructor(props) {
    super(props);

    this.textString = props.textString;

    const fonts = fs.readdirSync(path.resolve('app/fonts'));
    this.fontURI = (props.fontURI != null) ? props.fontURI : path.resolve(`app/fonts/${math.pickRandom(fonts)}`);
    console.log(this.fontURI);

    try {
      const font = opentype.loadSync(this.fontURI);

      const pathObject = font.getPath(this.textString, 0, 0, 20);
      this.textPath = pathObject.toPathData(4);

      this.design.scope.activate();
      this.element = new paper.CompoundPath(this.textPath);
      this.element.fillColor = new paper.Color('#101010');

      if (props.size != null) {
        this.matchSize(this.size);
      } else if (props.width != null) {
        this.matchWidth(this.width);
      } else if (props.height != null) {
        this.matchHeight(this.height);
      } else {
        this.sizeToRandom();
      }

      this.moveToRandom();

      if (this.initOrder === 'front') {
        this.element.bringToFront();
      } else {
        this.element.sendToBack();
      }
    } catch (e) {
      console.log(`Error loading font: ${this.fontURI}`);
      console.log(e);
    }
  }

  sizeToRandom() {
    let width = math.randomInt(
      math.round(this.design.width / 10),
      this.design.width
    );
    width = math.round(width / 10) * 10;
    this.matchWidth(width);
  }

  updateTextPath() {
    opentype.load(this.fontURI, (err, font) => {
      if (err) {
        console.log(`There was a problem with this font: ${this.fontURI}`);
        return null;
      }

      const pathObject = font.getPath(this.textString, 0, 0, 20);
      return pathObject.toPathData(4);
    });
  }

  moveTo(x, y) {
    const target = new paper.Point(x, y);
    const current = this.element.bounds.center;
    const delta = target.subtract(current);
    this.element.translate(delta);
  }
}

module.exports = TextPath;
