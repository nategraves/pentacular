const React = require('react');
const Tool = require('./Tool');

const Design = require('./Design');

class RectangleTool extends Tool {
  constructor(props) {
    super(props);

    this.addRectangle = this.addRectangle.bind(this);
    this.seedRectangles = this.seedRectangles.bind(this);
  }

  addRectangle() {
    const creative = this.creative;
    const enabledDesigns = creative.designs.filter((design) => design.state.liked);
    if (enabledDesigns.length > 0) {
      for (let i = 0; i < enabledDesigns.length; i += 1) {
        enabledDesigns[i].addRectangle(creative);
      }
    }
  }

  seedRectangles() {
    const creative = this.creative;
    for (let i = 0; i < 12; i += 1) {
      const design = new Design({ creative });
      design.addRectangle(creative);
      creative.designs.push(design);
    }
  }

  render() {
    return (
      <div className="text-center tool">
        <h2><i className="fa fa-square-o" /></h2>
        <button onClick={this.seedRectangles} className="text-button">Generate</button>
        <button onClick={this.addRectangle} className="text-button">Add</button>
      </div>
    );
  }
}

module.exports = RectangleTool;
