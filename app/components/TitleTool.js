const React = require('react');
const Tool = require('./Tool');
const TagsInput = require('react-tagsinput');

const Design = require('./Design');

class TitleTool extends Tool {
  constructor(props) {
    super(props);

    this.state = {
      tokens: []
    };

    this.handleChange = this.handleChange.bind(this);
    this.updateText = this.updateText.bind(this);
    this.addTitle = this.addTitle.bind(this);
    this.seedTitles = this.seedTitles.bind(this);
  }

  handleChange(tokens) {
    this.setState({ tokens });
  }

  clearTokens() {
    this.setState({ tokens: [] });
  }

  updateText(event) {
    this.setState({ value: event.target.value });
  }

  addTitle() {
    const creative = this.creative;
    const enabledDesigns = creative.designs.filter((design) => design.state.liked);
    if (enabledDesigns.length > 0) {
      for (let i = 0; i < enabledDesigns.length; i += 1) {
        enabledDesigns[i].addTitle(this.state.tokens);
      }
    }
    this.clearTokens();
  }

  seedTitles() {
    const creative = this.creative;
    for (let i = 0; i < 12; i += 1) {
      const design = new Design({ creative });
      design.addTitle(this.state.tokens);
      creative.designs.push(design);
    }
    this.clearTokens();
  }

  render() {
    return (
      <div className="text-center">
        <TagsInput value={this.state.tokens} onChange={this.handleChange} />
        <button onClick={this.seedTitles} className="text-button">Generate Text Options</button>
        <button onClick={this.addTitle} className="text-button">Add Text</button>
      </div>
    );
  }
}

module.exports = TitleTool;
