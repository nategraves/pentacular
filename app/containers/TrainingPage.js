// @flow
import { Link } from 'react-router-dom';

const React = require('react');
const TrainColors = require('../components/TrainColors');
const TrainFonts = require('../components/TrainFonts');

class TrainingPage extends React.Component {
  render() {
    return (
      <div>
        <Link to="/">Back</Link>
        <TrainColors />
        <TrainFonts />
      </div>
    );
  }
}

module.exports = TrainingPage;
