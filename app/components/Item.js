const math = require('mathjs');
const paper = require('paper');
const React = require('react');
const PropTypes = require('prop-types');

class Item extends React.Component {
  constructor(props) {
    super(props);

    if (this.constructor === Item) {
      throw new TypeError(`Cannot directly instantiate ${this.constructor.name} class`);
    }

    this.design = props.design;
    this.position = props.position;
    this.initOrder = props.initOrder;
  }

  // sendForward() {}
  // sendBack() {}

  center() {
    const target = this.creative.midCenter;
    this.moveTo(target.x, target.y);
    const current = this.element.bounds.center;
    const delta = target.subtract(current);
    this.element.translate(delta);
  }

  moveToPoint(target) {
    this.element.position = target;
  }

  moveTo(x, y) {
    this.element.position = new paper.Point(x, y);
  }

  moveToRandom() {
    let position = new paper.Point(this.design.width, this.design.height);
    const offset = -math.round(this.design.width / 5);
    position = position.multiply(paper.Point.random());
    position.x = math.round(position.x / 10) * 10;
    position.y = math.round(position.y / 10) * 10;
    this.element.position = position;
    while (!this.design.scope.view.bounds.contains(this.element.bounds, offset)) {
      position = new paper.Point(this.design.width, this.design.height);
      position = position.multiply(paper.Point.random());
      position.x = math.round(position.x / 10) * 10;
      position.y = math.round(position.y / 10) * 10;
      this.element.position = position;
    }
  }

  randomRGB() {
    const r = math.random();
    const g = math.random();
    const b = math.random();
    return new paper.Color(r, g, b);
  }

  randomRGBA() {
    const r = math.random();
    const g = math.random();
    const b = math.random();
    const a = math.random();
    return new paper.Color(r, g, b, a);
  }

  randomPosition() {
    let size = new paper.Size(this.design.width, this.design.height);
    size = size.multiply(paper.Point.random());
    size.width = math.round(size.width, 10);
    size.height = math.round(size.height, 10);
    return size;
  }

  randomSize() {
    let size = new paper.Size(this.design.width, this.design.height);
    size = size.multiply(paper.Size.random());
    size.width = math.round(size.width, 10);
    size.height = math.round(size.height, 10);
    return size;
  }

  sizeToRandom() {
    this.matchSize(this.randomSize());
  }

  matchSize(size) {
    const scaleWidth = size.width / this.element.bounds.width;
    const scaleHeight = size.height / this.element.bounds.height;
    this.element.scale(scaleWidth, scaleHeight);
    this.width = this.element.bounds.width;
    this.height = this.element.bounds.height;
  }

  matchWidth(width) {
    const scale = width / this.element.bounds.width;
    this.element.scale(scale);
    this.width = this.element.bounds.width;
    this.height = this.element.bounds.height;
  }

  matchHeight(height) {
    const scale = height / this.element.bounds.height;
    this.element.scale(scale);
    this.width = this.element.bounds.width;
    this.height = this.element.bounds.height;
  }

  sendToTop() {
    this.element.bringToFront();
  }

  sendToBottom() {
    this.element.sendToBack();
  }
}

Item.propTypes = {
  design: PropTypes.object.isRequired,
  position: PropTypes.object,
  initOrder: PropTypes.string
};

module.exports = Item;
