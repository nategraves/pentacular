import React from 'react';
import { Link } from 'react-router-dom';
import Creative from './Creative';


export default class Home extends React.Component {
  render() {
    return (
      <div>
        <Link to="/"><i className="fa fa-chevron-left"></i> Back</Link>
        <Creative />
      </div>
    );
  }
}
