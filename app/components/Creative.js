const PropTypes = require('prop-types');
const React = require('react');
const request = require('request');

const TitleTool = require('./TitleTool');
const RectangleTool = require('./RectangleTool');
const CircleTool = require('./CircleTool');

class Creative extends React.Component {
  constructor(props) {
    super(props);

    this.addRectangle = this.addRectangle.bind(this);
    this.addCircle = this.addCircle.bind(this);
    this.pruneDesigns = this.pruneDesigns.bind(this);
    this.saveDesigns = this.saveDesigns.bind(this);
    this.selectAll = this.selectAll.bind(this);
    this.selectNone = this.selectNone.bind(this);

    this.name = props.name;
    this.width = props.width;
    this.height = props.height;
    this.designs = [];
    this.maxCanvasSide = 400;
    this.scaleFactor = this.calculateScaleFactor(this.width, this.height, this.maxCanvasSide);
    // this.canvasWidth = this.width * this.scaleFactor;
    // this.canvasHeight = this.height * this.scaleFactor;
    this.canvasWidth = 400;
    this.canvasHeight = 400;
    this.rtl = true;
  }

  calculateScaleFactor(width, height, max) {
    let scale = 1;
    if (width > max) {
      scale = max / width;
    }

    if (height * scale > max) {
      scale = max / height;
    }

    return scale;
  }

  pruneDesigns() {
    const disabledDesigns = this.designs.filter((design) => !design.state.liked);
    for (let i = 0; i < disabledDesigns.length; i += 1) {
      disabledDesigns[i].canvas.remove();
    }
    const enabledDesigns = this.designs.filter((design) => design.state.liked);
    const designMatrix = this.designs.map((design) => [design.toMatrix(), design.state.liked]);
    console.log(designMatrix);
    /*
    request.post('http://localhost:5000', designMatrix, (error, response, body) => {
      if (!error && response.statusCode === 200) {
        console.log(body);
      } else {
        console.log(error);
      }
    });
    */
    this.designs = enabledDesigns;
  }

  saveDesigns() {
    for (let i = 0; i < this.designs.length; i += 1) {
      this.designs[i].export();
    }
  }

  selectAll() {
    const designs = this.designs.filter((design) => !design.state.liked);
    for (let i = 0; i < designs.length; i += 1) {
      designs[i].toggleEnabled();
    }
  }

  selectNone() {
    const designs = this.designs.filter((design) => design.state.liked);
    for (let i = 0; i < designs.length; i += 1) {
      designs[i].toggleEnabled();
    }
  }

  addRectangle() {
    if (this.designs.length > 0) {
      const enabledDesigns = this.designs.filter((design) => design.state.liked);
      for (let i = 0; i < enabledDesigns.length; i += 1) {
        enabledDesigns[i].addRectangle(this);
      }
    }
  }

  addCircle() {
    if (this.designs.length > 0) {
      const enabledDesigns = this.designs.filter((design) => design.state.liked);
      for (let i = 0; i < enabledDesigns.length; i += 1) {
        enabledDesigns[i].addCircle(this);
      }
    }
  }

  render() {
    return (
      <div>
        <div className="main">
          <div className="flex-item">
            <TitleTool creative={this} />
          </div>
          <div className="flex-item">
            <RectangleTool creative={this} />
          </div>
          <div className="flex-item">
            <CircleTool creative={this} />
          </div>
        </div>
        <div id="sketches">
          <div className="text-center">
            <button onClick={this.pruneDesigns} className="sketch-button">Prune</button>
            <button onClick={this.saveDesigns} className="sketch-button">Save</button>
            <button onClick={this.selectAll} className="sketch-button">Select All</button>
            <button onClick={this.selectNone} className="sketch-button">Select None</button>
          </div>
        </div>
      </div>
    );
  }
}

Creative.propTypes = {
  name: PropTypes.string,
  creative: PropTypes.object,
  design: PropTypes.object,
  position: PropTypes.object,
  width: PropTypes.number,
  height: PropTypes.number
};

module.exports = Creative;
